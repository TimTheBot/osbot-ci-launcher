import sys
import subprocess
import os
import json
import time
from time import gmtime, strftime

def get_number_of_servers(servers=None):
    if servers is None:
        servers = get_servers()
    return servers['pagination']['total']


def cmd(command):
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    str = process.stdout.read()
    print process.stderr.read()
    print str
    return str


def ssh_cmd(serverIp, command, bkg=False):
    ssh_precmd = "ssh -i id_rsa -oStrictHostKeyChecking=no -oConnectTimeout=3000 {2}@{0} '{1}' &".format(serverIp, command,
                                                                                       os.environ['BUILD_USER'])
    if bkg:
        subprocess.Popen(ssh_precmd, shell=True)
        return "Command started"
    return cmd(ssh_precmd)


def get_servers():
    get_servers_url = "https://api.godaddy.com/v1/cloud/servers"
    message_header = "Authorization: sso-key " + os.environ['KEY']
    get_all_servers = "curl " + get_os_str() + " -H \"Authorization: sso-key " + \
                      os.environ['KEY'] + \
                      "\" https://api.godaddy.com/v1/cloud/servers"

    response = cmd(get_all_servers)
    jData = json.loads(response)
    return jData


def check_servers():
    servers = get_servers()

    if get_number_of_servers(servers) == 0:
        create_new_server = "curl "+ get_os_str() + " -H \"Authorization: sso-key " + os.environ['KEY'] + "\" -H \"Content-Type: application/json\" -d '{\"image\": \"ubuntu-git-maven-sshkeys\", \"spec\": \"tiny\", \"username\": \"" + os.environ['BUILD_USER'] + "\", \"sshKeyId\":\"" + os.environ['SSH_KEY_ID'] + "\", \"hostname\": \"test-ci\"}' https://api.godaddy.com/v1/cloud/servers"
        server = cmd(create_new_server)
        time.sleep(60)
        return check_servers()
    else:
        status = servers['results'][0]['status']
        if status == "RUNNING":
            return servers['results'][0]['publicIp'], servers['results'][0]['serverId']
        elif status in ["NEW", "BUILDING", "STARTING", "DESTROYING", "DESTROYED"]:
            print "Server is: " + status + ", waiting 2 seconds..."
            time.sleep(2)
            return check_servers()
        elif status in ["STOPPED", "STOPPING"]:
            print "Server is shut down! Restarting..."
            restart_server = "curl " + get_os_str() +" -H \"Authorization: sso-key {1}\" -X POST https://api.godaddy.com/v1/cloud/servers/{0}/start".format(servers['results'][0]['serverId'], os.environ['KEY'])
            cmd(restart_server)
            time.sleep(5)
            return check_servers()


def get_os_str():
    os_option = "-s"
    if os.name == "nt":
        os_option = "-k"
    return os_option


def signal_self_destruct(ip, id):
    ssh_cmd(ip, "ls | grep selfdestruct || echo 'Selfdestruct initiated at: {0}' > selfdestruct".format(strftime("%Y-%m-%d %H:%M:%S", gmtime())))
    self_destruct = "nohup sleep 3000 && curl -H \"Authorization: sso-key {0}\" -X POST https://api.godaddy.com/v1/cloud/servers/{1}/destroy > shutdown.txt".format(
        os.environ['KEY'], id
    )
    return ssh_cmd(ip, self_destruct, bkg=True)


def connect_and_launch(ip):
    connect_and_launch = "cd osbot-python-CI-scripts && git pull && python JarBuilder.py {0}".format(sys.argv[1])
    return ssh_cmd(ip, connect_and_launch)

if __name__ == "__main__":
    ip, id = check_servers()

    signal_self_destruct(ip, id)

    print connect_and_launch(ip)
